﻿using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace ModeloUsuarios
{
    class VistaAzul
    {

        public VistaAzul()
        {
            ModeloUsuarios.Instancia.alCrear += this.AlCrearUsuario;
            ModeloUsuarios.Instancia.alModificar += this.AlModificarUsuario;
            ModeloUsuarios.Instancia.alEliminar += this.AlEliminarUsuario;
        }

        void AlCrearUsuario(Usuario usu)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("usuario creado: " + usu.ToString());
            Console.BackgroundColor = ConsoleColor.Black;
        }
        void AlModificarUsuario(Usuario bsq, Usuario mdf)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("usuario modific: " + bsq.ToString() 
                + " nuevos datos: " + mdf.ToString());
            Console.BackgroundColor = ConsoleColor.Black;
        }
        void AlEliminarUsuario(Usuario usu)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("usuario eliminado: " + usu.ToString());
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
