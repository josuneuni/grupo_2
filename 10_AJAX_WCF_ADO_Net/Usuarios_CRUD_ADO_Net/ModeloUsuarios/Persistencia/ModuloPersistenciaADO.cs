﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{

    public enum Entorno
    {
        Ninguno = 0,
        Produccion = 1,
        Desarrollo = 2,
        Preproduccion = 3,
    }

    public class ModuloPersistenciaADO
    {
        string CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu.mdf;Integrated Security=True;Connect Timeout=30";

        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();

        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;
            Console.WriteLine("El entorno es: " + entorno);
            if (entorno == (Entorno.Desarrollo))
            {
                Console.WriteLine("ESTAMOS EN DESARROLLO CAMBIAMOS CONEX_BD");
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu_desarrollo.mdf;Integrated Security=True;Connect Timeout=30";

            }else if (entorno == (Entorno.Produccion))
            {
                Console.WriteLine("ESTAMOS EN DESARROLLO CAMBIAMOS CONEX_BD");
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu_produccion.mdf;Integrated Security=True;Connect Timeout=30";

            }
            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            listaUsuarios = Leer();
        }

        public int? getIdFromEmail(string email)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id FROM Usuario WHERE email = @email";
                    comando.Parameters.AddWithValue("@email", email);

                    SqlDataReader lectorDR = comando.ExecuteReader();
                    lectorDR.Read();
                    return lectorDR.GetInt32(0);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd getIdFomrEmail" + ex.Message);
                return null;
            }
        }

        public Usuario Crear(Usuario u)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "INSERT INTO Usuario(nombre,email,altura,edad,activo) VALUES (@nombre, @email, @altura, @edad, @activo)";
                    comando.Parameters.AddWithValue("@nombre", u.Nombre);
                    comando.Parameters.AddWithValue("@email", u.Email);
                    comando.Parameters.AddWithValue("@altura", u.Altura.ToString().Replace(",", "."));
                    comando.Parameters.AddWithValue("@edad", u.Edad);
                    comando.Parameters.AddWithValue("@activo", u.Activo? 1:0);

                    //comando.CommandText = "INSERT INTO Usuario(nombre,email,altura,edad,activo) VALUES ('" + u.Nombre + "', '" + u.GetEmail() + "', " + u.Altura.ToString().Replace(",", ".") + ", " + u.Edad + ", '"+u.Activo.ToString().ToUpper()+"')";
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                    {
                        throw new Exception("No ha hecho bien insert bbdd" + comando.CommandText);
                    }

                    comando.CommandText = "SELECT id FROM Usuario WHERE email=@email2";
                    comando.Parameters.AddWithValue("@email2", u.Email);

                    Console.WriteLine(comando.CommandText);
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    lectorDR.Read();
                    u.SetId(lectorDR.GetInt32(0));
                    return u;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(u.GetEmail());
                Console.Error.WriteLine("Error creando usuario" + ex.Message);
                listaUsuarios = new List<Usuario>();
                throw new Exception("Error creando usuario");
            }
        }
        
      /*  public  void Guardar(List<Usuario> usus)
        {
            try
            {

                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    
                    foreach (Usuario u in listaUsuarios)
                    {
                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "INSERT INTO Usuario(nombre,email,altura,edad,activo) VALUES ('"+u.Nombre+"', '" + u.GetEmail() + "', "+u.Altura+", "+u.Edad+", 0)";
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                        {
                            throw new Exception("No ha hecho bien insert bbdd" + comando.CommandText);
                        }
                    }
                }
                Console.WriteLine("Leidos en bbdd guardar: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd guardando usuarios" + ex.Message);
                listaUsuarios = new List<Usuario>();
            }

        }*/

        public bool Eliminar(int entero)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "DELETE  FROM Usuario WHERE Usuario.id=@id";
                    comando.Parameters.AddWithValue("@id", entero);

                    if (comando.ExecuteNonQuery() != 1)
                    {
                        throw new Exception("No se ha afectado una unica fila");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error eliminando 1 elemento" + ex.Message);
                return false;
            }
            return true ;
        }

        public  List<Usuario> Leer()
        {
            try {

                SqlConnection conexion;
                using (conexion=new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, email, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    listaUsuarios.Clear();

                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.SetId(lectorDR.GetInt32(0));
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float)(double)lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(4);
                        usuario.Activo = (bool) lectorDR[5];
                        usuario.SetEmail(lectorDR[2].ToString());

                        listaUsuarios.Add(usuario);
                    }
                }
                Console.WriteLine("Leidos en bbdd leer: " + listaUsuarios.Count);
            } catch(Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd leyendo" + ex.Message);
                listaUsuarios = new List<Usuario>();
            }            
            return listaUsuarios;
        }

        public IList<Usuario> LeerTodo()
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, email, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    listaUsuarios.Clear();
                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.SetId(lectorDR.GetInt32(0));
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float)(double)lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(4);
                        usuario.SetEmail(lectorDR.GetString(2));
                        usuario.Activo = lectorDR.GetBoolean(5);

                        listaUsuarios.Add(usuario);
                    }
                }
                Console.WriteLine("Leidos en bbdd leer: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd leyendo" + ex.Message);
                listaUsuarios = new List<Usuario>();
            }
            return listaUsuarios;
        }

        public Usuario LeerUno(int entero)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, email, edad, altura, activo FROM Usuario WHERE Usuario.id=@id";
                    comando.Parameters.AddWithValue("@id", entero);

                    SqlDataReader lectorDR = comando.ExecuteReader();
                    //conexion.Close();
                    lectorDR.Read();
                    return new Usuario(lectorDR.GetInt32(0), lectorDR.GetString(1), lectorDR.GetString(2), lectorDR.GetByte(3), (float) lectorDR.GetDouble(4));
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leer 1 elemento" + ex.Message);
                return null;
            }
        }

        public bool LeerConfirmar(string nombre)
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public Usuario Modificar(int id, Usuario u)
        {
            try
            {
                SqlConnection conexion;
                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "UPDATE Usuario SET nombre=@nombre, email=@email, altura=@altura, edad=@edad, activo=@activo WHERE id=@id";
                    comando.Parameters.AddWithValue("@nombre", u.Nombre);
                    comando.Parameters.AddWithValue("@email", u.Email);
                    comando.Parameters.AddWithValue("@altura", u.Altura.ToString().Replace(",", "."));
                    comando.Parameters.AddWithValue("@edad", u.Edad);
                    comando.Parameters.AddWithValue("@activo", u.Activo ? 1 : 0);
                    comando.Parameters.AddWithValue("@id", id);
                    if (comando.ExecuteNonQuery() != 1)
                    {
                        throw new Exception("No se ha afectado una unica fila");
                    }
                }
                Console.WriteLine(listaUsuarios);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error modificando 1 elemento" + ex.Message);
                throw new Exception("Error modificando usuario");
            }
            u.Id = id;
            return u;
        }
    }
}
