﻿using System;

namespace ModeloUsuarios
{
    public class Usuario 
    {
        protected int? id;
        protected string nombre;
        protected string email;
        protected int edad;
        protected float altura;
        protected bool activo;

        public Usuario(int id, string nombre, string email, int edad, float altura)
        {
            Id = id;
            this.email = email;
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario(int id, string nombre, string email, int edad, float altura, bool activo)
        {
            Id = id;
            this.email = email;
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
            Activo = activo;
        }

        public Usuario(string nombre, string email, int edad, float altura)
        {
            Console.WriteLine("Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura);
            Id = null;
            Email = email;
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario(string nombre, int edad, float altura)
        {
            Console.WriteLine("Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura);
            Id = null;
            if (nombre != null)
            {
                email = nombre.ToLower().Replace(' ', '_') + "@email.es";
            }
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario() { }

        public override string ToString()
        {
            return "Id: " + Id+ " Nombre: " + nombre + " Email: " + email + " Edad: " + edad + " Altura: " + altura;
        }

        public void SetId(int? id)
        {
            this.Id = id;
        }

        public int? GetId()
        {

            return this.Id;
        }

        public string GetNombre()
        {
            return nombre;
        }

        public void SetEmail(string email)
        {
            this.email = email;
        }

        public string GetEmail()
        {
            return email;
        }
        
        public void SetNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    nombre = value;
                }
                else
                {
                    throw new Exception("Nombre vacio o null");
                }
            }
        }

        public bool Activo
        {
            get
            {
                return activo;
            }
            set
            {
                activo = value;
            }
        }


        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if (value > 0 && value < 120 )
                {
                    edad = value;
                }
                else
                {
                    throw new Exception("Edad invalida");

                }
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if (value > 0.1f && value < 3f)
                {
                    altura = value;
                }
                else
                {
                    throw new Exception("Altura invalida");

                }
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    email = value;
                }
                else
                {
                    throw new Exception("Email vacio o null");
                }

            }
        }

        public int? Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public override bool Equals(object usuario)
        {
            Usuario u = (Usuario)usuario;
            if (base.Equals(usuario))
                return true;
            else
            {
                return (this.Nombre.Equals(u.Nombre)
                    && this.Edad.Equals(u.Edad)
                    && this.Altura.Equals(u.Altura)
                    && this.Email.Equals(u.Email)
                    && this.Id.Equals(u.Id));
            }
        }
    }
}
