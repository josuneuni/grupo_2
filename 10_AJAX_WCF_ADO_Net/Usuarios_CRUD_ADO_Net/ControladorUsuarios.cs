﻿using Ejemplo03_MVC;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    class ControladorUsuarios
    {
        //GestionUsuarios es el modelo e implementa IModeloGenerico.
        //Cambiar GestionUsuarios por cualquier otra cosa para cambiar de modelo.
        //Ya cambiaremos los parámetros según pida el nuevo modelo. Si fuera empleado y pidiese sueldo (por ejemplo), aquí nos lo pide y la vista ni se entera.
        ModeloUsuarios modelo;

        public ControladorUsuarios()
        {
            this.modelo = ModeloUsuarios.Instancia;

        }

        public Usuario CrearUno(string nombre, int edad, float altura)
        { 
            return modelo.Crear(nombre, edad, altura);
            
        }
        public Usuario MostrarUno(string nombre)
        {
            return modelo.LeerUno(nombre);
        }
        public IList<Usuario> MostrarTodos()
        {
            return modelo.LeerTodos();
        }
        public Usuario ModificarUno(string nombreBusq, string nombre, int edad, float altura)
        {
            return modelo.Modificar(nombreBusq, nombre, edad, altura);
        }
        public bool EliminarUno(string nombre)
        {
            return modelo.Eliminar(nombre);
        }
    }
}
