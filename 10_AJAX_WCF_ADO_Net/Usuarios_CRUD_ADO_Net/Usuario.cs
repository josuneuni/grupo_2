﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo01_Encapsulacion;

namespace Ejercicio01_Encapsulacion
{
    public class Usuario
    {
        
        string nombre;
        int edad;
        float altura;

        public Usuario()
        {
        }

        public Usuario(string nombre, int edad, float altura)
        {
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public override string ToString()
        {
            return "Nombre: " + Nombre + ", edad: " + Edad + ", altura: " + altura;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    Nombre = "SIN NOMBRE";
                }
                else
                {
                    nombre = value;
                }
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }

            set
            {
                edad = value > 0 ? value : 1;
            }

        }

        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value > 0.1 ? value : (float) 0.1;
            }
        }

        public string  GetNombre()
        {
            return Nombre;
        }

        public void SetNombre(string nuevo_nombre)
        {
            Nombre = nuevo_nombre;
        }

    

    }
}
