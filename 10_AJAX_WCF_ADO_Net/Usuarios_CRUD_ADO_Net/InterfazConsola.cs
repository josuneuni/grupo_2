﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class InterfazConsola
    {
        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.WriteLine("Introduzca " + nombreDato);
            Boolean valor_nulo;
            do
            {
                varDato = Console.ReadLine();
                valor_nulo = String.IsNullOrEmpty(varDato);
                if (valor_nulo) Console.WriteLine("Debe de introducir " + nombreDato);
            }

            while (valor_nulo);
        }
        public static void PedirNum<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.WriteLine(nombreDato);

            bool esValorOk;
            do
            {
                string str = Console.ReadLine();

                if (typeof(Tipo) == typeof(int))
                {
                    int numero;
                    esValorOk = int.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                    esValorOk = float.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double numero;
                    esValorOk = double.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else
                {
                    throw new FormatException("ERROR: Tipo de dato no valido");
                    /*  esValorOk = true;
                     varNum = default(Tipo);
                     Console.Error.WriteLine("ERROR: Tipo de dato no valido");*/
                    // Lanzamos adrede una excepción
                    //TODO: excepción
                    // throw new FormatException("ERROR: Tipo de dato no valido");
                }
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");

            }
            while (!esValorOk);
        }
        public static void PedirInt(string nombreDato, out int varNum)
        {
            Console.WriteLine(nombreDato);

            bool esValorOk;
            do
            {
                string str = Console.ReadLine();

                esValorOk = int.TryParse(str, out varNum);
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esValorOk);
        }
    }


}

