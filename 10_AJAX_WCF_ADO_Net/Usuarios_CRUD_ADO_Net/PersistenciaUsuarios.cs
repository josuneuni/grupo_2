﻿using Ejercicio01_Encapsulacion;
using System;
using System.IO;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    class PersistenciaUsuarios
    {
        const string FICH_USUARIOS = "../../../fich_usuarios.json";

        public PersistenciaUsuarios()
        {
            ModeloUsuarios.Instancia.alCrear += this.AlCrearUsuario;
            ModeloUsuarios.Instancia.alEliminar += this.AlEliminarUsuario;
            ModeloUsuarios.Instancia.alInicializar = CargarDatosJSON;
            ModeloUsuarios.Instancia.alModificar = AlModificarUsuario;
        }
        void AlCrearUsuario(Usuario usu)
        {
            List<Usuario> usuarios = CargarDatosJSON();
            usuarios.Add(usu);
            GuardarDatosJSON(usuarios);
        }
        void AlEliminarUsuario(Usuario usu)
        {
            List<Usuario> usuarios = CargarDatosJSON();
            usuarios.RemoveAll((Usuario u) =>  u.Nombre == usu.Nombre);
            GuardarDatosJSON(usuarios);
        }
        void AlModificarUsuario(Usuario bsq, Usuario mdf)
        {
            List<Usuario> usuarios = CargarDatosJSON();
            // bucle foreach en su formato funcional
            usuarios.ForEach((Usuario u) =>
            {
                if (u.Nombre == bsq.Nombre)
                {
                    u.Nombre = mdf.Nombre;
                    u.Edad = mdf.Edad;
                    u.Altura = mdf.Altura;
                }
            });
            GuardarDatosJSON(usuarios);
        }
        List<Usuario> CargarDatosJSON()
        {
            List<Usuario> usuarios;
            if (File.Exists(FICH_USUARIOS))
            {
                usuarios = JsonSerializer.Deserialize<List<Usuario>>(File.ReadAllText(FICH_USUARIOS));
            }
            else
            {
                usuarios = new List<Usuario>();
            }
            return usuarios;
        }
        void GuardarDatosJSON(List<Usuario> usuarios)
        {

            JsonSerializerOptions jso = new JsonSerializerOptions();
            jso.WriteIndented = true;
            File.WriteAllText(FICH_USUARIOS, JsonSerializer.Serialize(usuarios, jso));
        }
    }
}
