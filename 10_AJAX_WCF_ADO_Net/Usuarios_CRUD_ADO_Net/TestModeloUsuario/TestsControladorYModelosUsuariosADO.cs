using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace TestModeloUsuario
{
    public class TestsmodeloYModelosUsuariosADO
    {
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistencia;
        Usuario usu1;

        void LimpiarJSON()
        {
            //Eliminar los usuarios creados en este test
           


        }
        void ResetearModeloYCtrl_ConReflection ()
        {
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        void EliminarTodosUsuarios()
        {
            int minId = 320;
            List<Usuario> lista= moduloPersistencia.Leer();
            foreach(Usuario usu in lista)
            {
                int? idUsuario = usu.GetId();
                if (idUsuario >= minId)
                {
                    moduloPersistencia.Eliminar((int)idUsuario);
                }
            }
        }

        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        [TearDown]
        public void AlTerminarTest()
        {
            // ResetearModeloYCtrl_ConReflection();
            EliminarTodosUsuarios();
            if ((new Random()).Next(3) == 0)
                LimpiarJSON();
        }
        
        void CrearUsuariosValidos()
        {
            usu1 = new Usuario("hola", "hola@", 27, 1.5f);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 33, 2));
            modelo.Crear(new Usuario("qtal", 45, 2));
        }
           
        [Test]
        public void TestCrearValidos()
        {
            int nusuarios = modelo.LeerTodos().Count;
            CrearUsuariosValidos();
            modelo.Crear(new Usuario("adios2", "pacopico@email.es", 17, 2.2f));
            modelo.Crear(new Usuario("qtal2", "pacopico@email.espana", 20, 2.4f));


            Assert.AreEqual(17, modelo.LeerUno((int) moduloPersistencia.getIdFromEmail("pacopico@email.es")).Edad);
            Assert.AreEqual(2.4f, modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("pacopico@email.espana")).Altura);
            Assert.AreEqual("hola", modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("hola@")).Nombre);
           // Assert.Null(modelo.LeerUno((int) moduloPersistencia.getIdFromEmail("fakeemalil@xd")));
            Assert.AreEqual(nusuarios + 5, modelo.LeerTodos().Count, "Mal creados los usuarios");
            Assert.IsTrue(modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("hola@")).Equals(new Usuario((int)moduloPersistencia.getIdFromEmail("hola@"),"hola","hola@", 27, 1.5f)));
        }

        [Test]
        public void TestCrearInvalidos()
        {
            int nusuarios = modelo.LeerTodos().Count;            
            Assert.Throws<Exception>(() => modelo.Crear(new Usuario(null, 400, 27)));
            Assert.Throws<Exception>(() => modelo.Crear(new Usuario("adios2", "pacopico@email.es", -3, 2.2f)));
            Assert.IsNull(modelo.Crear(null));

            Assert.AreEqual(modelo.LeerTodos().Count, nusuarios, "Mal creados los usuarios");
        }

            [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();
            int nusuarios = modelo.LeerTodos().Count;

            Usuario usu1mod = new Usuario((int)moduloPersistencia.getIdFromEmail("hola@"),"hola", "hola@xd", 90, 1.7f);
            Assert.AreEqual(modelo.Modificar((int)moduloPersistencia.getIdFromEmail("hola@"), usu1mod), usu1mod); 

            Assert.AreNotEqual(modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("hola@xd")), usu1);
            Assert.AreEqual(modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("hola@xd")), usu1mod);

            Assert.Throws<Exception>(() => modelo.Modificar(nusuarios + 100, new Usuario("NoExisto", 88, 88)));

        }
       

        [Test]
        public void TestEliminar()
        {
            int nusuarios = modelo.LeerTodos().Count;
            Console.WriteLine("Numero usuarios previo: " + nusuarios);
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", "aDios@", 43, 2));
            modelo.Crear(new Usuario("qtal", "hola2@", 40, 2));
            Usuario usu = modelo.LeerTodos()[nusuarios + 1];
            Console.WriteLine(usu.ToString());
            Assert.AreEqual(true, modelo.Eliminar((int)moduloPersistencia.getIdFromEmail("aDios@")));
            foreach(Usuario usuario in modelo.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu, "El usuario no se ha borrado correctamente");
            }
            Assert.AreEqual(nusuarios + 2, modelo.LeerTodos().Count);
            Assert.AreEqual(false, modelo.Eliminar(nusuarios + 100), "Error eliminando usuario no existente");
            Assert.AreEqual(nusuarios + 2, modelo.LeerTodos().Count);
        }

        [Test]
        public void TestLeerUno()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", "aDios@", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.Null(modelo.LeerUno(4));
            Assert.NotNull(modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("aDios@")));
            Assert.IsInstanceOf<Usuario>(modelo.LeerUno((int)moduloPersistencia.getIdFromEmail("aDios@")));
       
        }
        
        [Test]
        public void TestLeerTodos()
        {
            int nusuarios = modelo.LeerTodos().Count;
            Assert.AreEqual(nusuarios, modelo.LeerTodos().Count);
            modelo.Crear(new Usuario("adios", "aDios@", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.AreEqual(nusuarios + 2, modelo.LeerTodos().Count);
            modelo.Eliminar((int)moduloPersistencia.getIdFromEmail("aDios@"));
            Assert.AreEqual(nusuarios + 1, modelo.LeerTodos().Count);

        }
    }

}