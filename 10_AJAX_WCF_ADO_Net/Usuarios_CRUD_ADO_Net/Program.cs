﻿using Ejemplo01_Encapsulacion;
using Ejemplo03_MVC;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;


namespace ModeloUsuarios
{
    class Program
    {
        VistaUsuarios vu;
        PersistenciaUsuarios pu;
        VistaAzul vaz;

        Program()
        {
            double uno = 0.1, dos = 0.2;
            Console.WriteLine("0.1 + 0.2 = " + (uno + dos));

            vaz = new VistaAzul();
            pu = new PersistenciaUsuarios();
            ControladorUsuarios controlador = new ControladorUsuarios();
            vu = new VistaUsuarios(controlador);
        }
        static void Main()
        {
            Program program = new Program();
            program.vu.Menu();
        }
    }
}
