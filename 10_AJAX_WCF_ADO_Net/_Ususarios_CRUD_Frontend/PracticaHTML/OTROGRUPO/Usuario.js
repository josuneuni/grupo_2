
class Usuario{
           
    constructor(nombre,edad,altura,act, email){
        
        if((typeof(nombre) !== "string") || (typeof(email) !== "string") ||(typeof(edad) !== "number") ||(typeof(altura) !== "number") ||(typeof(act) !== "boolean"))
            throw "Argumento no valido";

        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura; 
        this.act = act;
        this.email = email;
    }

}
