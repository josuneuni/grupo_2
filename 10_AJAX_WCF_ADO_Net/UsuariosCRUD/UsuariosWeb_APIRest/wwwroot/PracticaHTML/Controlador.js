
class Controlador {
    usuarios = [];
    constructor() {
        //this.usuarios = JSON.parse(window.localStorage.getItem("usuarios"));
        modelo = new Modelo(this.usuarios);
        vista = new Vista(this);
        modelo.OnCreate = vista.mostrar;
        modelo.OnUpdate = vista.mostrar;
        modelo.OnDelete = vista.mostrar;
        modelo.datosIncorrectos = vista.datosIncorrectos;
        modelo.datosCorrectos = vista.datosCorrectos;

    }
    crear(nombre, edad, altura, activo, email) {
        let usuario = new Usuario(nombre, edad, altura, activo, email)
        modelo.crear(usuario);
    }
/*
    devolverId() {
        return modelo.devolverId();
    }*/

    actualizarTabla() {
        return modelo.actualizarTabla();
    }
    listarUsuarios() {
        return modelo.listaUsuarios();
    }

    modificarUsuario(usuOld, usuNew) {
        modelo.modificar(usuOld, usuNew);
    }

    eliminar(usuario) {
        modelo.eliminar(usuario);
    }

}


window.onload = function() {
    controlador = new Controlador();
    modelo.leer();
    let btnAnadir = document.getElementById("btn-anadir");
    btnAnadir.addEventListener("click", vista.crear.bind(vista));
};

