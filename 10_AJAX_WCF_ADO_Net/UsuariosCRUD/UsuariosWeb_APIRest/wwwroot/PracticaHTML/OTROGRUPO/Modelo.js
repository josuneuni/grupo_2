var arrayUsuarios;
class Modelo {

    constructor() {
        this.OnCreate = null;
        this.OnUpdate = null;
        this.OnDelete = null;
        this.datosIncorrectos = null;
        this.datosCorrectos = null;

    }
    comprobarTipo(usuario) {
        if (typeof (usuario.nombre) === "string" && !usuario.nombre == "" && isNaN(parseInt(usuario.nombre))) {
            if (!isNaN(parseInt(usuario.edad))) {
                if (!isNaN(parseFloat(usuario.altura))) {
                    return true;
                }
            }
        }
        else
            return false;
    }
   /* leer() {
        arrayUsuarios = JSON.parse(window.localStorage.getItem("lista-usuarios"));
        if (arrayUsuarios !== null)
            this.OnCreate(arrayUsuarios);
        else
            arrayUsuarios = [];
    }*/
    leer() {
        //arrayUsuarios = JSON.parse(window.localStorage.getItem("lista-usuarios"));

        let promesaAJAX = fetch("/api/usuarios");
        promesaAJAX.then((respuesta) => {
            return respuesta.json()
        }).then(objWF => {                          //Callback de la llamada as�ncrona    
            console.log(objWF)
            arrayUsuarios = objWF;
            if (arrayUsuarios !== null) {
                this.OnCreate(arrayUsuarios);
            } else {
                arrayUsuarios = [];
            }
        });
    }


    /*crear(usuario) {
        if(this.comprobarTipo(usuario)) {
            arrayUsuarios.push(usuario);
            this.guardarDatosModelo(arrayUsuarios);
            this.OnCreate(arrayUsuarios);
            this.datosCorrectos();
        } else {
            this.datosIncorrectos();
        }
    }*/
    crear(usuario) {
        if (this.comprobarTipo(usuario)) {

            let opcionesPOST = {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                headers: {
                    "Content-Type": "application/json"
                },
                body: `{
                            "nombre": "${usuario.nombre}", 
                            "email": "${usuario.email}",
                            "edad": ${usuario.edad},
                            "activo": ${usuario.activo},
                            "altura": ${usuario.altura}
                        }`
            };
            let promesaAJAX = fetch("/api/Usuarios/crear/", opcionesPOST);
            promesaAJAX.then((respuesta) => {
                return respuesta.json();
            }).then(objTempWF => {
                console.log(objTempWF);
                arrayUsuarios.push(objTempWF);
                this.OnCreate(arrayUsuarios);
            });
        } else {
            this.datosIncorrectos();
        }
    }

    /*modificar(usuariomodificar, nuevousuario) {
        if (this.comprobarTipo(nuevousuario)) {
            arrayUsuarios[arrayUsuarios.indexOf(usuariomodificar)] = nuevousuario;
            this.guardarDatosModelo(arrayUsuarios);
            this.OnUpdate(arrayUsuarios);
            this.datosCorrectos();
        } else {
            this.datosIncorrectos();
            this.OnUpdate(arrayUsuarios);

        }
    }*/
    modificar(usuariomodificar, nuevousuario) {
        let opcionesPUT = {
            method: "PUT",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
            },
            body: `{
                        "nombre": "${nuevousuario.nombre}", 
                        "email": "${nuevousuario.email}",
                        "edad": ${nuevousuario.edad},
                        "activo": ${nuevousuario.activo},
                        "altura": ${nuevousuario.altura}
                    }`
        };
        let promesaAJAX = fetch("/Modificar/" + usuariomodificar.id, opcionesPUT);
        promesaAJAX.then((respuesta) => {
            return respuesta.json();
        }).then(objTempWF => {
            console.log(objTempWF);
            arrayUsuarios[arrayUsuarios.indexOf(usuariomodificar)] = objTempWF;
            this.OnUpdate(arrayUsuarios);
        });
    }



    eliminar(usuarioeliminar) {
        let opcionesDEL = {
            method: "DELETE",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let promesaAJAX = fetch("/eliminar/" + usuarioeliminar.id, opcionesDEL);
        promesaAJAX.then((respuesta) => {
            return respuesta.json()
        }).then(objWF => {
            arrayUsuarios.splice(arrayUsuarios.indexOf(usuarioeliminar), 1);
            this.OnDelete(arrayUsuarios);
        });

    }

    /*
    validarUsuario(){
        let campoNombre = document.getElementById("nombre");    
        let nombre = campoNombre.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
        if (nombre == "") {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca un nombre";
            return null;
        }
        let edad = parseInt(document.getElementById("edad").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(edad)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una edad correcta";
            return null;
        }
        let altura = parseFloat(document.getElementById("altura").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(altura)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una altura";
            return null;
        }
        let activo = document.getElementById("activo").checked;
        document.getElementById("mensaje-validacion").className = "no_ver";
        return new Usuario(nombre,edad,altura,activo);
    }*/
}