﻿using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{

    [ApiController]
    [Route("api/[controller]")]

    public class UsuariosController : ControllerBase
    {
        IModeloGenerico<Usuario> modeloUsuario;

        private readonly ILogger<UsuariosController> _logger;


        public UsuariosController(ILogger<UsuariosController> _logger)
        {
            this.modeloUsuario = ModeloUsuario.Instancia;
        }

        [HttpPost("crear/")]
        public Usuario Crear(Usuario nuevoObj)
        {
            Usuario u = modeloUsuario.Crear(nuevoObj);
            return u;
        }

        [HttpGet]
        public IList<Usuario> LeerTodos()
        {
            IList<Usuario> l = modeloUsuario.LeerTodos();
            return l;
        }

        [HttpDelete("/eliminar/{entero}")]
        public bool Eliminar(int entero)
        {
            return modeloUsuario.Eliminar(entero);
        }

        [HttpGet("{entero}")]
        public Usuario LeerUno(int entero)
        {
            return modeloUsuario.LeerUno(entero);
        }
        [HttpGet("/LeerConfirmar/{nombre}")]
        public bool LeerConfirmar(string nombre)
        {
            return modeloUsuario.LeerConfirmar(nombre);
        }
        [HttpPut("/Modificar/{i}")]
        public Usuario Modificar(int i, Usuario usuario)
        {
            return modeloUsuario.Modificar(i, usuario);
        }
    }
}
