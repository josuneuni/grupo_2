import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuarios-activos',
  templateUrl: './usuarios-activos.component.html',
  styleUrls: ['./usuarios-activos.component.css']
})
export class UsuariosActivosComponent implements OnInit {
  @Input()
  usuariosAct: string="";
  @Input()
  usuariosInact: string="";


  constructor() { }

  ngOnInit(): void {
  }

}
