import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuarios-edad',
  templateUrl: './usuarios-edad.component.html',
  styleUrls: ['./usuarios-edad.component.css']
})
export class UsuariosEdadComponent implements OnInit {
  @Input()
  adultos: string="";
  @Input()
  menores: string="";
  constructor() { }

  ngOnInit(): void {
  }

}
