import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UsuariosActivosComponent } from './usuarios-activos/usuarios-activos.component';
import { UsuariosEdadComponent } from './usuarios-edad/usuarios-edad.component';

@NgModule({
  declarations: [
    AppComponent,
    UsuariosActivosComponent,
    UsuariosEdadComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
