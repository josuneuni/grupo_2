import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class ServicioUsuariosService {

  private arrayUsuarios: Array<Usuario> = [];
  private url: string = "http://localhost:54879/api/usuarios";

  constructor(private clienteHTTP: HttpClient) {
    let observ: Observable<Array<Usuario>>;

    observ = this.clienteHTTP.get<Array<Usuario>>(this.url);
    observ.subscribe((datos: Array<Usuario>) =>{
      this.arrayUsuarios=datos;
    });
  }

  public getUsuarios(){
    
    return this.arrayUsuarios;
  }

  public usuActivos(){
    let activos=0;
    let inactivos=0;
    this.arrayUsuarios.forEach(usuario => {
      if(usuario.activo){
        activos++;
      }else{
        inactivos++;
      }
    });
    return [activos,inactivos];
  }

  public usuAdultos(){
    let adultos=0;
    let menores=0;
    this.arrayUsuarios.forEach(usuario => {
      if(usuario.edad>=18){
        adultos++;
      }else{
        menores++;
      }
    });
    return [adultos,menores];
  }
}
