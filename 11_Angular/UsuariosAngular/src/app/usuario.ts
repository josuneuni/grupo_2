export class Usuario {

    public nombre: string = "";
    public activo: boolean = false;
    public edad: number = 0;
    public altura: number = 0;
    public email: string = "";
    public id: number = 0;

}
