import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  template: `
  <p>trozo-web works!</p>
  <p>Mostrnado valor propiedad con interpolación (variable interpolada)</p>
  {{10+5 + propiedadClase  + contadorComp}}
  <!--Equivalente al \${expresion} de JS o de c#-->
  <!--Con los paréntesis vinculamos un evento con un método TS de la clase -->
  <input type="button" value="Aumentar" (click)="alPulsarBoton()"/>
  <p>Pulsado {{contadorComp}} veces</p>
  
  <span> Cambia el valor</span><input type="number" [(ngModel)]="contadorComp">
  
  `
})
export class BindingsComponent implements OnInit {

  
  propiedadClase:string;
  static contadosEstatico: number=0;
  contadorComp: number=0;
  arrayDeTextos: Array<string>;


  constructor() { 
    this.propiedadClase="...";
    this.contadorComp=1;
    this.arrayDeTextos=["relleno","cuajada","tarta de cuajada","bacalao ajoarriero"];

  }
  ngOnInit(): void {
    BindingsComponent.contadosEstatico ++;
    this.propiedadClase=`ngONInit es el primer método
    del cilo de vida del componente (ejecutado ${BindingsComponent.contadosEstatico} veces)`;
  }

  alPulsarBoton() : void{
    this.contadorComp++;
  }

}
