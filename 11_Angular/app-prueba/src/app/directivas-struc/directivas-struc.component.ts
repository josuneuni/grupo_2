import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas-struc',
  templateUrl: './directivas-struc.component.html',
  styleUrls: ['./directivas-struc.component.css']
})
export class DirectivasStrucComponent implements OnInit {

  contadorComp: number=0;
  arrayDeTextos: Array<string>;

  constructor() { 
    this.contadorComp=1;
    this.arrayDeTextos=["relleno","cuajada","tarta de cuajada","bacalao ajoarriero"];
  }

  ngOnInit(): void {
  }

}
